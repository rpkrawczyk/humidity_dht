#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>
#include <unistd.h>
#include <assert.h>

#define MAX_TIME 85
/*
 * According to http://pinout.xyz/pinout/pin13_gpio27 this is
 * GPIO27 (wiringPi 2).
 */
#define DEFAULT_DHT_PIN 2
#define MAX_TIMINGS 2200

enum Data_Modes {
  MODE_DHT11,
  MODE_DHT22
};

struct CLIConfig {
  int successful;
  int retries;
  int verbose;
  enum Data_Modes data_mode;
  int dht_pin;
};

int dht11_val[5]={0,0,0,0,0};

void ping_sensor(int dht_pin) {
  pinMode(dht_pin, OUTPUT);
  digitalWrite(dht_pin, LOW);
  delay(18);
  digitalWrite(dht_pin, HIGH);
  delayMicroseconds(40);
  pinMode(dht_pin, INPUT);
}


int dht11_read(int dht_pin, int data[5]) {
  uint8_t lststate = HIGH;
  uint8_t counter = 0;
  uint8_t j = 0, i;

  for(i = 0; i < 5; i++) {
     data[i] = 0;
  }
  ping_sensor(dht_pin);
  for(i = 0; i < MAX_TIME; i++) {
    counter=0;
    while(digitalRead(dht_pin) == lststate) {
      counter++;
      delayMicroseconds(1);
      if(counter == 255) break;
    }
    lststate=digitalRead(dht_pin);
    if(counter == 255)
       break;
    // top 3 transistions are ignored
    if((i >= 4) && (i % 2 == 0)){
      data[j / 8] <<= 1;
      if(counter > 16) {
        data[j / 8] |= 1;
      }
      j++;
    }
  }
  // verify checksum
  if((j >= 40) &&
     (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))) {
    //Checksum is OK.
  } else {
    return 0;
  }
  return 1;
}


/*! Read temperature according to mode.
 *
 * The temperatur is read and interpreted according to the mode. That is either as DHT11 or DHT22.
 *
 * \param dht_pin wiring pi pin number
 * \param dmode data mode
 * \param humidity pointer to humididy variable
 * \param temperature pointer to temperature variable
 * \return 1 = OK, 0 = error
 */
int dht11_interpret(int dht_pin, enum Data_Modes dmode, float *humidity, float *temperature) {
  int dht_data[5];
  int ret = -1;
  float tmp;

  if(dht11_read(dht_pin, dht_data) != 0) {
    switch(dmode) {
    case MODE_DHT11:
      *humidity = dht_data[0];
      *temperature = dht_data[2];
      break;
    case MODE_DHT22:
      *humidity = (dht_data[0] << 8 | dht_data[1]) / 10.0;
      tmp = dht_data[2] & 0x7f;
      if((dht_data[2] & 0x80) == 0) {
	tmp *= 256.0;
      } else {
	tmp *= -256.0;
      }
      *temperature = (tmp + dht_data[3]) / 10.0;
      break;
    }
    ret = 1;
  } else {
    ret = 00;
  }
  return ret;
}


int dht11_read_output(int dht_pin, enum Data_Modes dmode) {
  float temperature, humidity;
  int ret;

  ret = dht11_interpret(dht_pin, dmode, &humidity, &temperature);
  assert(ret >= 0);
  if(ret) {
    printf("Humidity %4.1f %%, Temperature %4.1f degrees Celsius\n", humidity, temperature);
    fflush(stdout);
  } else {
    //fprintf(stderr, "Invalid Data!!\n");
  }
  return ret;
}



void new_dht_read(int dht_pin) {
  int i;
  char timings[MAX_TIMINGS + 1];
  char *ptr;
  char *last;

  timings[MAX_TIMINGS] = '\0';
  ping_sensor(dht_pin);
  for(i = 0; i < MAX_TIMINGS; i++) {
    timings[i] = digitalRead(dht_pin) ? '#' : '.';
    delayMicroseconds(1);
  }
  printf("'%s'\n", timings);
  last = timings;
  for(ptr = timings; *ptr; ++ptr) {
    if(*ptr != *last) {
      printf("%u %c  ", ptr - last, *last);
      last = ptr;
    }
  }
  putchar('\n');
  fflush(stdout);
}


struct CLIConfig read_cli(int argc, char **argv) {
  static struct option long_options[] = {
    { "retries", required_argument, NULL, 'r' },
    { "wiring-pin", required_argument, NULL, 'p' },
    { 0 }
  };
  struct CLIConfig defaultopts = {
    1,
    5,
    0,
    MODE_DHT22,
    DEFAULT_DHT_PIN
  };
  int optc, optidx;

 parse_cli_loop:
  optc = getopt_long(argc, argv, "v12r:p:", long_options, &optidx);
  if(optc == -1) return defaultopts;
  switch(optc) {
  case 'r':
    defaultopts.retries = atoi(optarg);
    break;
  case 'v':
    defaultopts.verbose = 1;
    break;
  case '1':
    defaultopts.data_mode = MODE_DHT11;
    break;
  case '2':
    defaultopts.data_mode = MODE_DHT22;
    break;
  case 'p':
    defaultopts.dht_pin = atoi(optarg);
    break;
  case '?':
    defaultopts.successful = 0;
    break;
  default:
    fprintf(stderr, "Unknown option character $%02X.\n", optc);
  }
  goto parse_cli_loop;
}

void endless_reading_loop(int dht_pin, enum Data_Modes dmode) {
  unsigned int counter = 0;

 endless_loop:
  if((counter & 1) == 0) {
    dht11_read_output(dht_pin, dmode);
  } else {
    new_dht_read(dht_pin);
  }
  delay(16191);
  ++counter;
  goto endless_loop;
}

int main(int argc, char **argv) {
  struct CLIConfig cliconfig;
  int i;

  cliconfig = read_cli(argc, argv);
  if(!cliconfig.successful) return 1;
  if(wiringPiSetup() == -1) {
    perror("Wiring pi failed!");
    exit(2);
  }
  if(cliconfig.retries <= 0) {
    endless_reading_loop(cliconfig.dht_pin, cliconfig.data_mode);
  }
  while(cliconfig.retries-- > 0) {
    if(dht11_read_output(cliconfig.dht_pin, cliconfig.data_mode)) {
      break;
    } else {
      usleep(200245);
    }
  }
  if(cliconfig.retries < 0) fprintf(stderr, "Invalid data!\n");
  if(cliconfig.verbose != 0) {
    for(i = 0; i < 5; ++i) {
      printf("\t %d\n", dht11_val[i]);
    }
  }
  return cliconfig.retries >= 0 ? 0 : 10;
}

/* 
* Links:
* * https://github.com/adafruit/Adafruit_Python_DHT/blob/master/source/Raspberry_Pi_2/pi_2_dht_read.c
* * http://www.wurst-wasser.net/wiki/index.php/RaspberryPi_Humidity_and_Temperature_Sensor
* * http://wiringpi.com/pins/
*/
