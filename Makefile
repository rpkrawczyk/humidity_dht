#! /usr/bin/make -f

CFLAGS=-O2 -Wall -Wextra
LIBS=-lwiringPi

dhtread: dht11.o
	$(CC) $(CFLAGS) -o $@ $+ $(LIBS)

clean:
	rm -f *.o dhtread

distclean:
	$(MAKE) clean
	rm -f *~

.PHONY: clean distclean
